﻿namespace Problem2
{
    using System;

    static class Program
    {
        static void Main(string[] args)
        {
            long x = 0;
            long y = 1;
            long z = 0;
            long sum = 0;

            while (z < 4000000)
            {
                z = x + y;
                x = y;
                y = z;
                if (y % 2 == 0)
                {
                    sum += y;
                }
            }

            Console.WriteLine(sum);
            Console.ReadKey();
        }
    }
}
