﻿namespace Problem1
{
    using System;

    static class Program
    {
        static void Main(string[] args)
        {
            long sum = 0;
            for (long i = 1; i < 100000000; i++)
            {
                if (i % 3 == 0 || i % 5 == 0)
                {
                    sum += i;
                }
            }
            Console.WriteLine(sum);
            Console.ReadKey();
        }
    }
}
